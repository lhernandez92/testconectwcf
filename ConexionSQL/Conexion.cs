﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Xml;
using Newtonsoft.Json;

namespace ConexionSQL
{
    public class Conexion
    {
        private Config xmlConfig;
        private DataSet ds = new DataSet();
        //SQL parameters
        private string server, user, password, database, queryConection;
        private SqlConnection sqlCon = new SqlConnection();

        public Conexion(string route)
        {
            route += "config.xml";
            xmlConfig = new Config(route);
            this.ds = this.xmlConfig.GetXML();
            this.server = this.ds.Tables["SQLConection"].Rows[0]["hostname"].ToString();
            this.user = this.ds.Tables["SQLConection"].Rows[0]["user"].ToString();
            this.password = this.ds.Tables["SQLConection"].Rows[0]["password"].ToString();
            this.database = this.ds.Tables["SQLConection"].Rows[0]["database"].ToString();
            this.queryConection = "Data Source=" + server + ";Initial Catalog=" + database + "; Integrated Security=true";
            this.sqlCon.ConnectionString = this.queryConection;
        }

        public SqlConnection GetConection()
        {
            return this.sqlCon;
        }

        public string GetStatusConection()
        {
            return this.sqlCon.State.ToString();
        }

        public void ExecQuery(string query)
        {
            try
            {
                SqlCommand sqlCMD = new SqlCommand(queryConection, this.GetConection());
                sqlCMD.Connection.Open();
                sqlCMD.ExecuteNonQuery();
                sqlCMD.Connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error! -> " + ex.ToString());
            }
        }

        public DataRowCollection GetDataSet(string query)
        {
            DataSet sqlDS = new DataSet();
            SqlDataAdapter sqlDA = new SqlDataAdapter(query, this.GetConection());
            sqlDA.Fill(sqlDS);

            return sqlDS.Tables[0].Rows;
        }

        public string GetDataSetXML(string query)
        {
            DataSet sqlDS = new DataSet();
            SqlDataAdapter sqlDA = new SqlDataAdapter(query, this.GetConection());
            sqlDA.Fill(sqlDS);

            return sqlDS.GetXml();
        }

        public string GetDataSetJSON(string query)
        {
            DataSet sqlDS = new DataSet();
            SqlDataAdapter sqlDA = new SqlDataAdapter(query, this.GetConection());
            sqlDA.Fill(sqlDS);

            return JsonConvert.SerializeObject(sqlDS, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
