﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;

namespace ConexionSQL
{
    public class Config
    {
        private DataSet ds = new DataSet();
        public Config(string url)
        {
            try
            {
                this.ds.ReadXml(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error en la carga de la configuracion XML " + ex.ToString());
            }            
        }

        public DataSet GetXML()
        {
            return this.ds;
        }
    }
}
