﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Data;
using ConexionSQL;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace TestConectWCF.Controllers
{
    public class WCFController : Controller
    {
        private static string route = HostingEnvironment.MapPath("~/App_Data/");
        private ServiceSQLContext.Service1Client svc = new ServiceSQLContext.Service1Client();
        private Conexion objCon = new Conexion(route);
        // GET: WCF
        public ActionResult Index()
        {
            ViewBag.Alumnos = objCon.GetDataSet("SELECT * FROM alumno");
            return View();
        }

        public ActionResult CargarCurso()
        {
            string xml = svc.ListarCursos();
            XDocument xDoc = XDocument.Parse(xml);
            ViewBag.Cursos = xDoc;
            return View();
        }

        public JsonResult QueryWCF()
        {
            return Json("");
        }

        public ActionResult CargarAlumnoCurso(int id = 0)
        {
            string json = svc.ListarAlumnoCurso(id);
            var obj = JsonConvert.DeserializeObject<DataSet>(json);

            if (id > 0)
            {
                ViewBag.AlumnoCurso = obj.Tables[0].Rows;
                ViewBag.TotalRegistros = obj.Tables[0].Rows.Count;
            }
            else
            {
                ViewBag.TotalRegistros = obj.Tables[0].Rows.Count;
            }

            return View();
        }
    }
}