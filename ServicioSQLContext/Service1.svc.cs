﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using ConexionSQL;
using System.Web.Hosting;
using System.ServiceModel.Activation;

namespace ServicioSQLContext
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service1 : IService1
    {
        private static string ruta = HostingEnvironment.MapPath("~/App_Data/");
        private Conexion objCon = new Conexion(ruta);
        public string ListarCursos()
        {                       
            string xmlDS = objCon.GetDataSetXML("SELECT * FROM Curso");
            return  xmlDS;
        }

        public string ListarAlumnoCurso(int id)
        {
            string query = "SELECT AC.id as Id, A.Nombre as NombreAlumno, C.Nombre as NombreCurso, AC.Nota as Nota  FROM AlumnoCurso as AC INNER JOIN Alumno as A ON AC.Alumno_id = A.id " +
                "INNER JOIN Curso as C ON AC.Curso_id = C.id WHERE AC.Alumno_id=" + id;
            string jsonDS = objCon.GetDataSetJSON(query);
            return jsonDS;
        }

    }
}
